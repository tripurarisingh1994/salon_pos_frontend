import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TableComponent } from './table.component';
import { BookTableComponent, NbAddItemsDailogComponent, NbAddCustomerDialogComponent, NbDialogDiscountComponent, NbDialogPaymentModeComponent } from './book-table/book-table.component';
import { FoodCategoryResolver } from './food-cats.resolver.service';


const routes: Routes = [{
  path: '',
  component: TableComponent,
  children: [
    {
      path: '',
      component: BookTableComponent,
      resolve: { foodCategory: FoodCategoryResolver},
   },
  {
     path: 'book-table',
     component: BookTableComponent,
     resolve: { foodCategory: FoodCategoryResolver},
  }
],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  entryComponents: [
    NbAddCustomerDialogComponent,
    NbAddItemsDailogComponent,
    NbDialogDiscountComponent,
    NbDialogPaymentModeComponent
  ]
})
export class TableRoutingModule { }

export const routedComponents = [
  TableComponent,
  BookTableComponent,
  NbAddCustomerDialogComponent,
  NbAddItemsDailogComponent,
  NbDialogDiscountComponent,
  NbDialogPaymentModeComponent
];
