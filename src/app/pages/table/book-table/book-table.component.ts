import { Component, OnInit, AfterContentChecked, Input, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
// import { Location } from '@angular/common';
import { CommonService } from '../../../_service/common.service';
import { NbDialogRef, NbDialogService, NbToastrService } from '@nebular/theme';
import { debounceTime } from 'rxjs/operators';


/********************  Add Customer Modal  Start ************************/


@Component({
  selector: 'nb-dialog',
  template: `
    <nb-card accent="info" [style.width.px]="600" [style.height.px]="370">
      <nb-card-header><h4>{{ title }}</h4></nb-card-header>
      <nb-card-body>
          <div class="row">
            <div class="col-md-6">
                <label for="name">Name <span class="label-star">*</span></label>
                <input type="text" #name nbInput fullWidth id="name" placeholder="Enter the customer name">
            </div>
            <div class="col-md-6">
                <label for="email" class="email-label">Email </label>
                <input type="email" #email nbInput fullWidth id="email" placeholder="Enter the customer email">
            </div>
          </div>
          <div class="row" [style.margin-top.px]="24">
              <div class="col-md-6">
                  <label for="mobile">Mobile <span class="label-star">*</span></label>
                  <input type="tel" #mob nbInput fullWidth id="mobile" placeholder="Enter the customer mobile">
              </div>
          </div>
      </nb-card-body>
      <nb-card-footer>
        <div [ngStyle]="{'float':'right'}">
          <button nbButton hero status="danger" (click)="dismiss()">close</button>
          <button nbButton hero status="success" [disabled]="!name.value || !mob.value" (click)="saveCustomerDetails(name.value, email.value, mob.value)">Save</button>
        </div>
      </nb-card-footer>
    </nb-card>
  `,
  styles:[
    `
     label {
        color: #fff;
     }
     nb-card-footer div button:nth-child(2){
       margin: 0 10px;
     }

     nb-card-footer div button {
       cursor: pointer;
     }

    .label-star {
        color: #ff386a;
        font-size: 1.6em;
        font-weight: 650;
        margin-left: 1px;
    }

    .email-label {
      margin-top: 0.6em;
      font-size: 1.05em;
    }
    
    `
  ], 
})
export class NbAddCustomerDialogComponent {
  @Input() title: string;
  currentUser;

  constructor(protected ref: NbDialogRef<NbAddCustomerDialogComponent>,
              private commonService: CommonService,
              private toastrService: NbToastrService) {

      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  dismiss() {
    this.ref.close();
  }

  saveCustomerDetails(name, email, mob): void {
    this.commonService.savingCustomerDetails(this.currentUser.client_id, name, email, mob).subscribe(data=>{ 
      // console.log(data);
      if(data['message']=='success') {
        this.toastrService.success('Success','Data added successfully');
      }
      else {
        this.toastrService.warning('Warning', 'Getting problem in data saving'); 
      }
    },
    (err)=>console.log(err)
    );

    this.ref.close();
  }
}



/************************************** End Of Add Customer Modal ****************************************/


/****************************************  Add Items Modal  Start ****************************************/


@Component({
  selector: 'nb-dialog1',
  template: `
    <nb-card accent="warning" [style.width.px]="600" [style.height.px]="370">
      <nb-card-header><h4>{{ title }}</h4></nb-card-header>
      <nb-card-body>
          <div class="row">
            <div class="col-md-6">
                <label id="selectCat" for="category">Select Category <span class="label-star">*</span></label>
                <select class="form-control" id="selectCat" [(ngModel)]="selectCat">
                    <option value="0">-- Select Category --</option>
                    <option *ngFor="let categL of categoryList" [value]="categL.id">{{ categL.name }}</option>
                </select>
            </div>
            <div class="col-md-6">
                <label for="name">Item Name <span class="label-star">*</span></label>
                <input type="text" #itemName nbInput fullWidth id="email" placeholder="Enter the item name">
            </div>
          </div>
          <div class="row" [style.margin-top.px]="24">
              <div class="col-md-6">
                  <label for="price">Price <span class="label-star">*</span></label>
                  <input type="number" #price nbInput fullWidth id="price" placeholder="Enter the price">
              </div>

              <div class="col-md-6">
                  <label for="hsn_code">HSN Code<span class="label-star">*</span></label>
                  <input type="text" #hsn_code nbInput fullWidth id="hsn_code" placeholder="Enter the hsn code">
              </div>
          </div>

          <div class="row" [style.margin-top.px]="24">
              <div class="col-md-6">
                  <label for="gst_rate">GST Rate <span class="label-star">*</span></label>
                  <input type="number" #gst_rate nbInput fullWidth id="gst_rate" placeholder="Enter the gst rate">
              </div>
          </div>

      </nb-card-body>
      <nb-card-footer>
        <div [ngStyle]="{'float':'right'}">
          <button nbButton hero status="danger" (click)="dismiss()">close</button>
          <button nbButton hero status="success" [disabled]="!selectCat || !itemName.value || !price.value || !hsn_code.value || !gst_rate.value" (click)="saveItemCategoryWise(itemName.value, price.value, hsn_code.value, gst_rate.value)">Save</button>
        </div>
      </nb-card-footer>
    </nb-card>
  `,
  styles:[
    `
     label {
        color: #fff;
     }

     input {
      border: 2px solid #7659ff !important;
     }

     nb-card-footer div button:nth-child(2){
       margin: 0 10px;
     }

     nb-card-footer div button {
       cursor: pointer;
     }

    .label-star {
        color: #ff386a;
        font-size: 1.6em;
        font-weight: 650;
        margin-left: 1px;
    }

    .dropdown, .dropup, .btn-group {
      margin-bottom: 1rem;
    }

    select {
      height: 41px !important;
    }

    `
  ], 
})
export class NbAddItemsDailogComponent implements OnInit {
  @Input() title: string;

  categoryList = [];
  selectedCategory:number;
  selectCat: number = 0;

  constructor(private ref: NbDialogRef<NbAddItemsDailogComponent>,
              private commonService: CommonService,
              private toastrService: NbToastrService) {
  }

  ngOnInit() {

    this.getCategoryList();
  }

  getCategoryList(): void {
    this.commonService.getFoodCategory().subscribe(async data=> {
        // console.log(data);
        if(data['message'] === 'success') {
          this.categoryList = await data['data'];
        }
    });
  }


  dismiss() {
    this.ref.close();
  }

  saveItemCategoryWise(itemName, price, hsn_code, gst_rate): void {
    // console.log(itemName, price, this.selectCat);
    this.commonService.saveItemsAndItsPrice(this.selectCat, itemName, price, hsn_code, gst_rate).subscribe(data => {

      if(data['message'] === 'success') {
        this.toastrService.success('Success','Item created successfully'); 
      }
      else
      this.toastrService.warning('Warning','Item is not created, something went wrong');

    });

    this.ref.close();
  }
}



/************************************** End Of Add Items Modal ****************************************/


/**************************************  Start the Discount Modal ************************************/

@Component({
  selector: 'nb-discount-prompt',
  template: `
    <nb-card accent="warning">
      <nb-card-header>Discount</nb-card-header>
      <nb-card-body>
        <label for="email" class="email-label">Discount in % </label> <br />
        <input style="margin: 0 2.3rem;" #discount nbInput placeholder="Discount in %">
      </nb-card-body>
      <nb-card-footer>
        <button nbButton hero status="danger" (click)="cancel()">Cancel</button>
        <button nbButton hero status="success" [disabled]="!discount.value" (click)="applyDiscount(discount.value)">Apply</button>
      </nb-card-footer>
    </nb-card>
  `,
  styles: [`
    button {
      margin: 1rem;
      cursor:pointer !important;
    }
  `],
})

export class NbDialogDiscountComponent{
 
  constructor(protected dialogRef: NbDialogRef<NbDialogDiscountComponent>,
             private commonService: CommonService) {  }

  cancel(): void {
    this.dialogRef.close({});
  }

  applyDiscount(discount): void {
    const price = this.commonService.totalPriceOfFoodItems(discount);
    this.dialogRef.close({price: price, discount: discount});
  }


}


/**************************************  End the Discount Modal ************************************/


/**************************************  Start the Tax Modal ************************************/

@Component({
  selector: 'nb-paymentmode-prompt',
  template: `
    <nb-card accent="warning">
      <nb-card-header>Payment Mode</nb-card-header>
      <nb-card-body>
        <nb-select placeholder="Payment Mode" [(ngModel)]="_paymentMode">
            <nb-option value="cash">Cash</nb-option>
            <nb-option value="card">Card</nb-option>
        </nb-select>
      </nb-card-body>
      <nb-card-footer>
        <button nbButton hero status="danger" (click)="cancel()">Cancel</button>
        <button nbButton hero status="success" [disabled]="!_paymentMode" (click)="paymentMode()">Apply</button>
      </nb-card-footer>
    </nb-card>
  `,
  styles: [`
    button {
      margin: 1rem;
      cursor:pointer !important;
    }
  `],
})

export class NbDialogPaymentModeComponent {
 
  _paymentMode:any;
  constructor(protected dialogRef: NbDialogRef<NbDialogPaymentModeComponent>) {  }

  cancel(): void {
    this.dialogRef.close();
  }

  paymentMode(): void {
    this.dialogRef.close(this._paymentMode);
  }


}


/**************************************  End the Tax Modal ************************************/


@Component({
  selector: 'ngx-book-table',
  templateUrl: './book-table.component.html',
  styleUrls: ['./book-table.component.scss']
})

export class BookTableComponent implements OnInit,  AfterContentChecked {

  loading: boolean = false;
  food_cat_lists = [];
  food_items = [];
  foodItems_Added_In_Cart:Array<Object>=[];
  totalPrice:number = 0; 
  afterDiscountPriceIs:number = null;


  dateTime: string;   // save for print bill

  filteredCustomerList = [];    // to store the searched result of customers

  isSelectCustomer: boolean = false;

  filteredFoodItemsList = [];   // to store the searched result of food items 

  selectedCustomerData = {};   // store the selected customer name from search filter list

  @ViewChild('searchItem') searchItemIn:ElementRef;
  currentUser: any;
  gs:any;

  // _tax:number = 0;        // storing the tax
  _discount:number = 0;   // storing the discount
  paymentMode:string = ''; 


  constructor(private _route: ActivatedRoute,
              private commonService: CommonService,
              private dialogService: NbDialogService,
              private toastrService: NbToastrService) {

    // console.log('BookTableComponent');
    this.dateTime = new Date().toLocaleString();
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.gs = JSON.parse(localStorage.getItem('gs'));
   }

  ngOnInit() {
    this.getFoodCategoryList();
  }

  getFoodCategoryList(): void {
    if(this._route.snapshot.data['foodCategory'].message == 'success') {

      this.food_cat_lists = this._route.snapshot.data['foodCategory'].data;
      this.getFoodItemsByFoodCatId(this.food_cat_lists[0].id);
    }
    else {
      console.log(this._route.snapshot.data['foodCategory'].message)
    }
  }

  getFoodItemsByFoodCatId(id): void {
    this.loading = true;
    this.commonService.getFoodItemsByFoodCategory(id).subscribe(data => {
      //  console.log(data['data']);
       this.food_items = data['data'];
       this.loading = false;
    });
  }

  /** Adding Food Items in the cart  */

  foodItemsAddToCart(item): void {
    this.afterDiscountPriceIs = null;
     let cart;
     this.commonService.getFoodItemPriceByFoodItemId(item.id).subscribe( async  data => {

         cart = {
          id    : parseInt(item.id),
          name  : item.name,
          qty   : 1,
          price : await parseFloat(data['data'][0].price)
        }
          await this.commonService.items_cart.push(cart);

          this.foodItems_Added_In_Cart = this.commonService.items_cart;

          this.totalPrice = this.commonService.totalPriceOfFoodItems();
     });

    //  console.log("foodItems_Added_In_Cart ",this.foodItems_Added_In_Cart);

  }

  /** Delete cart item by item id */

  deleteFoodItem_FromCart(cart): void {
    // console.log(cart);
    const index = this.commonService.items_cart.findIndex(items => items.id == cart.id);
    // console.log("index ",index)
    this.commonService.items_cart.splice(index,1);
    
    this.afterDiscountPriceIs = null;
    this.totalPrice = this.commonService.totalPriceOfFoodItems();

    // console.log("deleteFoodItem_FromCart ",this.commonService.items_cart);
  }


/** to change the Quantity of the food items in cart*/
   toChangeQty(id, signFlag): void {

    if(signFlag === 'plus') {
      this.afterDiscountPriceIs = null;
      const index = this.commonService.items_cart.findIndex(items => items.id == id);    
      this.commonService.items_cart[index].qty++;
    }
    else {
      this.afterDiscountPriceIs = null;
      const index = this.commonService.items_cart.findIndex(items => items.id == id);    
      this.commonService.items_cart[index].qty--;
    }

    // console.log("update cart ",this.commonService.items_cart);
  }

  /** Open the modal for add customer */
  openAddCustomerModal(): void {
    // console.log("add customer");
    this.dialogService.open(NbAddCustomerDialogComponent, {
      context: {
        title: 'Add Customer',
      },
    });
  }


  openAddItemModal(): void {
    this.dialogService.open(NbAddItemsDailogComponent, {
      context: {
        title: 'Add Item',
      }
    })
  }


  /** Search the customer  */

  searchCustomer(query: string): void {
    // console.log(query);
    this.filteredCustomerList = [];
    query!='' && this.commonService.searchCustomer(this.currentUser.client_id, query).pipe(debounceTime(500)).subscribe( data=> {
      // console.log(data);
      if(data['message']=='success') 
          this.filteredCustomerList = data['data'];
    }, (err)=> console.log(err));
  }

  /** Select Customer From Search Filter List */
  selectCustomerFromSearchFilter(filterCust): void {

    this.selectedCustomerData = filterCust;

    this.filteredCustomerList = [];

    this.isSelectCustomer = true;

    // console.log(this.selectedCustomerData);
  }

  /** Remove the Selected Customer from Search Filter List */

  removeSelectedCustomer(): void {
    this.selectedCustomerData = {};
    this.isSelectCustomer = false;
  }

  
  /**
   * Search Food Items By Item name
   */

   searchFoodItemsByName(query): void {
    // console.log(query);
    this.filteredFoodItemsList = [];
    query!='' && this.commonService.searchFoodItems(query).pipe(debounceTime(500)).subscribe(data => {
        // console.log(data);
        if(data['message'] === 'success')
          this.filteredFoodItemsList = data['data'];
    }, (err) => console.log(err));
   }


   /** Select Food Item from Search filter list */

   selectFoodItemFromSearchFilter(filterItem): void {
    // console.log("filterItem ",filterItem);

    this.foodItemsAddToCart(filterItem);
    this.searchItemIn.nativeElement.value = "";   // Reset the search input box
    
    this.filteredFoodItemsList = [];
   }


  ngAfterContentChecked() {
    this.foodItems_Added_In_Cart = this.commonService.items_cart;
    if(this.afterDiscountPriceIs != null)
      this.totalPrice = this.afterDiscountPriceIs
    else
      this.totalPrice = this.commonService.totalPriceOfFoodItems();
  }


  printBillToCustomer(): void {

    if(this.foodItems_Added_In_Cart.length > 0) {

        let billToPrint = document.getElementById('printBill').innerHTML;
        let newWindow = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');

        newWindow.document.open();
        newWindow.document.write(`
        <html>
            <head>
              <title>${this.gs.shop_name}</title>
              <style>
              @media print {  
                @page {
                  size: A5;
                }
              }
              </style>
            </head>
            <body onload="window.print();window.close()">${billToPrint}   
            </body>
          </html>
        `);

         newWindow.document.close();

         this.commonService.insertNewInvoice( parseInt(this.selectedCustomerData['id']), this.totalPrice, this.paymentMode, this._discount).subscribe( async data => {

          if(data['message'] == 'success') {
            this.toastrService.success('Success','Invoice created successfully'); 

                this.foodItems_Added_In_Cart = this.commonService.items_cart = [];
          }
          else{
            this.toastrService.warning('Warning','Invoice is not created, something went wrong');

                this.foodItems_Added_In_Cart = this.commonService.items_cart = [];
          }

         }, (err)=> console.log(err));

    }
    else {
        console.log('no Data for this table and category');
    }

  }

  /** Apply Discount */
  discount(): void {
    if(this.foodItems_Added_In_Cart.length > 0) {
        this.dialogService.open(NbDialogDiscountComponent).onClose.subscribe( data => {
          
          if(Object.entries(data).length === 0 && data.constructor === Object) return
          else {
            this.afterDiscountPriceIs = data.price;
            this._discount = parseInt(data.discount)
          }
        });
    }
  }

  /** Apply Payment Mode */
  payment(): void {
    if(this.foodItems_Added_In_Cart.length > 0) {
        this.dialogService.open(NbDialogPaymentModeComponent).onClose.subscribe( data => {
          // console.log(data);
          this.paymentMode = data
      });
    }
  }

}
