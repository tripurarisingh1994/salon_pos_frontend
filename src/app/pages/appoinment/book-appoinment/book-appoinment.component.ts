import { Component, OnInit, ChangeDetectionStrategy, Input, ChangeDetectorRef } from '@angular/core';
import { CalendarEvent } from 'angular-calendar';
import { format, setHours, setMinutes } from 'date-fns';
import { colors } from '../demo-utils/colors';
import { CommonService } from '../../../_service/common.service';
import { NbToastrService, NbDialogRef, NbDialogService } from '@nebular/theme';
import { debounceTime } from 'rxjs/operators';


/****************************************  Add Customer Modal  Start ********************************************/


@Component({
  selector: 'nb-dialog',
  template: `
    <nb-card accent="info" [style.width.px]="600" [style.height.px]="370">
      <nb-card-header><h4>{{ title }}</h4></nb-card-header>
      <nb-card-body>
          <div class="row">
            <div class="col-md-6">
                <label for="name">Name <span class="label-star">*</span></label>
                <input type="text" #name nbInput fullWidth id="name" placeholder="Enter the customer name">
            </div>
            <div class="col-md-6">
                <label for="email" class="email-label">Email </label>
                <input type="email" #email nbInput fullWidth id="email" placeholder="Enter the customer email">
            </div>
          </div>
          <div class="row" [style.margin-top.px]="24">
              <div class="col-md-6">
                  <label for="mobile">Mobile <span class="label-star">*</span></label>
                  <input type="tel" #mob nbInput fullWidth id="mobile" placeholder="Enter the customer mobile">
              </div>
          </div>
      </nb-card-body>
      <nb-card-footer>
        <div [ngStyle]="{'float':'right'}">
          <button nbButton hero status="danger" (click)="dismiss()">close</button>
          <button nbButton hero status="success" [disabled]="!name.value || !mob.value" (click)="saveCustomerDetails(name.value, email.value, mob.value)">Save</button>
        </div>
      </nb-card-footer>
    </nb-card>
  `,
  styles:[
    `
     label {
        color: #fff;
     }
     nb-card-footer div button:nth-child(2){
       margin: 0 10px;
     }

     nb-card-footer div button {
       cursor: pointer;
     }

    .label-star {
        color: #ff386a;
        font-size: 1.6em;
        font-weight: 650;
        margin-left: 1px;
    }

    .email-label {
      margin-top: 0.6em;
      font-size: 1.05em;
    }
    
    `
  ], 
})
export class NbAddCustomerDialogComponent {
  @Input() title: string;
  currentUser;

  constructor(protected ref: NbDialogRef<NbAddCustomerDialogComponent>,
              private commonService: CommonService,
              private toastrService: NbToastrService) {
                
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  dismiss() {
    this.ref.close();
  }

  saveCustomerDetails(name, email, mob): void {
    this.commonService.savingCustomerDetails(this.currentUser.client_id, name, email, mob).subscribe(data=>{ 
      console.log(data);
      if(data['message'] === 'success') {
        this.toastrService.success('Success','Data added successfully');
      }
      else {
        this.toastrService.warning('Warning', 'Getting problem in data saving'); 
      }
    },
    (err)=>console.log(err)
    );

    this.ref.close();
  }
}



/************************************** End Of Add Customer Modal ****************************************/



/**************************************** Appointment Modal  Start **************************************/


@Component({
  selector: 'nb-dialog-appointment',
  template: `
    <nb-card accent="info" [style.width.px]="600" [style.height.px]="390">
      <nb-card-header><h4>Appointment Details</h4></nb-card-header>
      <nb-card-body>
          <div class="row">

            <div class="col-md-4">
                <label for="name">Customer Name</label>
                <p>{{appointment_details.cust_name}}</p>
            </div>

            <div class="col-md-4">
                <label for="email">Customer Email </label>
               <p>{{appointment_details.cust_email}}</p>
            </div>

            <div class="col-md-4">
              <label for="mobile">Customer Mobile</label>
              <p>{{appointment_details.cust_mob}}</p>
            </div>

          </div>
          <div class="row">

              <div class="col-md-4">
                  <label for="mobile">Service Category</label>
                  <p>{{appointment_details.serv_category_name}}</p>
              </div>

              <div class="col-md-4">
                  <label for="mobile">Service Name</label>
                  <p>{{appointment_details.service_name}}</p>
              </div>

              <div class="col-md-4">
                  <label for="mobile">Service Price</label>
                  <p>{{appointment_details.service_price | currency:'INR'}}</p>
              </div>

          </div>

          <div class="row">

            <div class="col-md-4">
                <label for="mobile">Booked Date</label>
                <p>{{appointment_details.booked_date}}</p>
            </div>

            <div class="col-md-4">
                <label for="mobile">Service Start Time</label>
                <p>{{appointment_details.serv_start_time | uppercase}}</p>
            </div>

            <div class="col-md-4">
                <label for="mobile">Service End Time</label>
                <p>{{appointment_details.serv_end_time}}</p>
            </div>

      </div>

      <div class="row">

          <div class="col-md-4">
              <label for="mobile">Staff </label>
              <p>{{appointment_details.staff_name}}</p>
          </div>

          <div class="col-md-4">
              <label for="mobile">Resources </label>
              <p>{{appointment_details.res_name}}</p>
          </div>


      </div>

      </nb-card-body>
      <nb-card-footer>
        <div [ngStyle]="{'float':'right'}">
          <button nbButton hero status="danger" (click)="dismiss()">close</button>
        </div>
      </nb-card-footer>
    </nb-card>
  `,
  styles:[
    `
     label {
        color: #fff;
     }
     nb-card-footer div button:nth-child(2){
       margin: 0 10px;
     }

     nb-card-footer div button {
       cursor: pointer;
     }
    
    `
  ], 
})
export class AppointmentDialogComponent implements OnInit{
  @Input() title: string;
  // currentUser;
  appointment_details = []

  constructor(protected ref: NbDialogRef<AppointmentDialogComponent>,
              private commonService: CommonService,
              private toastrService: NbToastrService) {
                
        // this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      //  console.log(this.commonService.booked_appointment_details)

  }
  ngOnInit() {
    // console.log(this.title)
    const _index = this.commonService.booked_appointment_details.findIndex(data => data.appointment_id == this.title )
     this.appointment_details = this.commonService.booked_appointment_details[_index]
  }



  dismiss() {
    this.ref.close();
  }

}



/************************************** Appointment Details Modal ****************************************/




@Component({
  selector: 'ngx-book-appoinment',
  templateUrl: './book-appoinment.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./book-appoinment.component.scss']
})
export class BookAppoinmentComponent implements OnInit {

  isHiddenFillApponment:boolean = false;
  isSelectCustomer: boolean     = false;
  isSelectService: boolean      = false;
  
  customerList                = [];       // to store the searched result of customers
  temp_CustomerList           = [];       // temporerly store customer
  selectedCustomerData        = {};       // store the selected customer name from search filter list
 
  resourcesList               = [];       //  store the resources  

  staffList                   = [];       // store the staff list

  temp_ServiceList            = [];       // store the templorely service list
  serviceList                 = [];       // store the service list
  selectedServiceData         = {};       // store the selected service from service list
  
  selectedResource:number     = 0; 
  selectedStaff:number        = 0;

  selectedBookTime:string     = '';       // store the booking time
  setServiceDuration:number   = 15;      // store the servicing time for paticular customer by default 15 Min   

  appoinment_cart = [];
  
  view: string = 'day';
  
  viewDate: Date = new Date();
  
  currentUser;

  events: CalendarEvent[] = []
  
  constructor(private dialogService: NbDialogService,
              private commonService: CommonService,
              private toastrService: NbToastrService,
              private changeDetector: ChangeDetectorRef) {

      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnInit() {
    this.gettingResources();
    this.gettingStaffList();
   
  }

  ngAfterContentInit() {
    this.gettingAppointmentDetails();
  }

  /** Open Appoinment  */
  openModal(event): void {

    // console.log(event.target.className);
    if(event.target.className == 'cal-hour-segment cal-after-hour-start ng-star-inserted') {
      this.isHiddenFillApponment = true;
      this.selectedBookTime = event.target.textContent;
    }
    else {
      return
    }
    // this.bookedDate = this.viewDate.toString();
  }

  /** Getting The resources */
  gettingResources(): void {
    this.commonService.gettingTheResources().subscribe(data=> {
      if(data['message'] === 'success') {
        this.resourcesList = data['data'];
      }
    });
  }

  /** Getting The Staff List */

  gettingStaffList(): void {
    this.commonService.gettingTheStaff().subscribe(data=> {
      if(data['message'] === 'success') {
        this.staffList = data['data'];
      }
    })
  }

  /** Search The Service */
  searchService(query: string): void {
    this.serviceList = [];
    // this.temp_ServiceList = [];
    query!='' && this.commonService.gettingItemsDetails(this.currentUser.client_id, query).pipe(debounceTime(500)).subscribe(data=> {
      console.log(data);
      if(data['message'] === 'success') {
        this.temp_ServiceList = data['data'];
      }
    }, (err)=> console.log(err));
  }

  onCloseBtn(): void {
    this.isHiddenFillApponment = false;
  }

    /** Open the modal for add customer */
    openAddCustomerModal(): void {
      // console.log("add customer");
      this.dialogService.open(NbAddCustomerDialogComponent, {
        context: {
          title: 'Add Customer',
        },
      });
    }

    /** Search the customer  */

  searchCustomer(query: string): void {
    this.customerList = [];
    // this.temp_CustomerList = [];
    query!='' && this.commonService.searchCustomer(this.currentUser.client_id, query).pipe(debounceTime(500)).subscribe(data=> {
      console.log(data);
      if(data['message'] === 'success') {
        this.temp_CustomerList = data['data'];
      }
    }, (err)=> console.log(err));
  }

  /** Select Customer From Search Filter List */
  selectCustomerFromSearchFilter(filterCust): void {

    this.selectedCustomerData = filterCust;

    this.temp_CustomerList    = [];
    this.customerList         = [];

    this.isSelectCustomer = true;

    console.log(this.selectedCustomerData);
  }

  /** Select The Service from the Service List */
  selectServiceFromServiceList(serviceL): void {
    this.selectedServiceData = serviceL;

    this.temp_ServiceList = [];
    this.serviceList      = [];

    this.isSelectService = true;

    console.log("selected Service Data", this.selectedServiceData);
  }

  /** Remove the Selected Customer from Search Filter List */

  removeSelectedCustomer(): void {
    this.selectedCustomerData = {};
    this.isSelectCustomer = false;
  }

/** Remove the selected service from service List */
  removeSelectedService(): void {
    this.selectedServiceData = {};
    this.isSelectService = false;
  }
  
  ngAfterContentChecked() {
    this.customerList = this.temp_CustomerList;
    this.serviceList  = this.temp_ServiceList;

    this.appoinment_cart = this.commonService.appointment_details;
  }

  /** Push in the appoinment_details array */

  pushInAppoinmentDetails(): void {

   if(this.commonService.appointment_details.length > 0) {

     const index = this.commonService.appointment_details.findIndex(appoinment => appoinment.serviceDetails['item_id'] == this.selectedServiceData['item_id'])
      console.log(index);

      if(index > -1) {
        this.toastrService.warning('Warning','This Item is already in list');
      }
      else {
        this.pushAppoinment();
      }
    }
   else {
    this.pushAppoinment();
   }

   this.isSelectService = false;
   this.selectedServiceData = {};
  }

   t24to12Convert(time):string {
    // Check correct time format and split into components
    time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
  
    if (time.length > 1) { // If time format correct
      time = time.slice (1);  // Remove full string match value
      time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
      time[0] = +time[0] % 12 || 12; // Adjust hours
    }
    return time.join (''); // return adjusted time or original string
  }

  t12to24Convert(time12h):string {

    const [time, modifier] = time12h.split(' ');

    let [hours, minutes] = time.split(':');

    if (hours === '12') {
        hours = '00';
    }

    if (modifier.toUpperCase() === 'PM') {
        hours = parseInt(hours, 10) + 12;
    }

    return hours + ':' + minutes;
  }

  pushAppoinment():void {
    const staffId   = this.staffList.findIndex(staff => staff.id == this.selectedStaff);

    const staffName = this.staffList[staffId].staff_name;

    let _t = this.selectedBookTime.trim().split(' ');

    if(_t[0].length === 2) 
      _t[0] = _t[0]+':00';
    else if(_t[0].length === 1)
      _t[0] = '0'+_t[0]+':00';

      let  new_d = new Date (new Date().toDateString() + ' ' + this.t12to24Convert(_t.join(' ')))
      
      let _new_d = new Date ( new_d );
      
      _new_d.setMinutes(new_d.getMinutes() + this.setServiceDuration);
      
      let _h = _new_d.getHours().toString();
      let _m = _new_d.getMinutes().toString();
      
      if(_h.length === 1)
      _h = '0'+_h;
      
      if(_m.length === 1)
      _m = '0'+_m;
      
      
    let appoinment_obj = {
      customerDetails: this.selectedCustomerData,
      serviceDetails: this.selectedServiceData,
      resource_id: this.selectedResource,
      staff_id: this.selectedStaff,
      staff_name: staffName,
      servicing_startTime: _t.join(' '),
      servicing_endTime: this.t24to12Convert(_h+':'+_m),
      booked_date: new Date(this.viewDate.toString()).getDate()+'/'+(new Date(this.viewDate.toString()).getMonth()+1)+'/'+new Date(this.viewDate.toString()).getFullYear(),
      booked_time_24H: _h+':'+_m
    }

    this.commonService.appointment_details.push(appoinment_obj);

    // console.log(this.commonService.appointment_details);
    appoinment_obj = null;
  }


  deleteAppoinment_Cart(cart): void {
    const index = this.commonService.appointment_details.findIndex(appoinment => appoinment.serviceDetails['category_id'] == cart.serviceDetails['category_id'] && appoinment.serviceDetails['item_id'] == cart.serviceDetails['item_id'])
    
    this.commonService.appointment_details.splice(index,1);
  }


  onAppoinmentDateChange(_date): void {

    if( new Date(_date).getDate() !== this.viewDate.getDate() || new Date(_date).getMonth() !== this.viewDate.getMonth() || new Date(_date).getFullYear() !== this.viewDate.getFullYear() ) {

      console.log('if')

      const __date = new Date(_date).getDate()+'/'+(new Date(_date).getMonth()+1)+'/'+new Date(_date).getFullYear();
      
      this.commonService.appointment_details.forEach(element => {
        element.booked_date = __date
      });

    }
    else {
      console.log('else')
      return
    }

  }

  /* Save the appoinment in table */
  saveAppoinment(): void {

    this.commonService.savingAppointmentDetails().subscribe(data => {
      // console.log(data);
      if(data['message'] === 'success') {
        let loadevents  = []

       data['data'].forEach(element => {
        //  console.log(element);
         const hhmm = element.booked_time_24H.split(':',2)
         const _book_date = element.booked_date.split('/')
         loadevents.push({id:element.appointment_id, title: element.cust_name+' '+element.cust_mob, start: setHours(setMinutes(new Date(format(new Date(_book_date[2], _book_date[1]-1, _book_date[0]), 'MM/DD/YYYY')), hhmm[1]), hhmm[0]), color: colors.blue } )
        });

       this.loadEvents(loadevents);

       this.commonService.booked_appointment_details = [];
       this.commonService.booked_appointment_details = data['data']

        // this.customerList                = [];       
        // this.temp_CustomerList           = [];       
        // this.selectedCustomerData        = {}; 
       
        // this.resourcesList               = [];      
      
        // this.staffList                   = [];    
        // this.temp_ServiceList            = [];   
        // this.serviceList                 = [];      
        // this.selectedServiceData         = {};      
        
        // this.selectedResource            = 0; 
        // this.selectedStaff               = 0;
      
        // this.selectedBookTime            = '';      

        this.isHiddenFillApponment          = false;
        // this.isSelectCustomer            = false;
        // this.isSelectService             = false;     

        // this.appoinment_cart             = [];

        this.changeDetector.detectChanges();

      }
    })
  }


  loadEvents(events= []) {
    console.log('load evnts functions')
    this.events = events
  }

  gettingAppointmentDetails() {
    this.commonService.fetchAppointmentDetails().subscribe(data=> {
      // console.log(data);

      if(data['message'] === 'success') {
        let loadevents  = []
       data['data'].forEach(element => {
         console.log(element.booked_date);
         const hhmm = element.booked_time_24H.split(':',2)
         const _book_date = element.booked_date.split('/')
         loadevents.push({id:element.appointment_id, title: element.cust_name+' '+element.cust_mob, start: setHours(setMinutes(new Date(format(new Date(_book_date[2], _book_date[1]-1, _book_date[0]), 'MM/DD/YYYY')), hhmm[1]), hhmm[0]), color: colors.blue } )
        //  console.log(loadevents);
       });

       this.loadEvents(loadevents);
      //  console.log('after load events')
       this.commonService.booked_appointment_details = [];
       this.commonService.booked_appointment_details = data['data']

       this.changeDetector.detectChanges();
      }
      else {
        return
      }
    })
  }

  eventClicked({ event }: { event: CalendarEvent }): void {
    // console.log('Event clicked', event);
    // console.log(event.id)
    this.dialogService.open(AppointmentDialogComponent, {
      context: {
        title: event.id,
      },
    });

  }
  
}
