import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppoinmentComponent } from './appoinment.component';
import { BookAppoinmentComponent, NbAddCustomerDialogComponent, AppointmentDialogComponent } from './book-appoinment/book-appoinment.component';


const routes: Routes = [{
  path: '',
  component: AppoinmentComponent,
  children: [
    {
      path: '',
      component:BookAppoinmentComponent ,
   },
  {
     path: 'book-appoinment',
     component:BookAppoinmentComponent ,
  }
],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  entryComponents: [
    NbAddCustomerDialogComponent,
    AppointmentDialogComponent
  ]
})
export class AppoinmentRoutingModule { }

export const routedComponents = [
  AppoinmentComponent,
  BookAppoinmentComponent,
  NbAddCustomerDialogComponent,
  AppointmentDialogComponent
];
