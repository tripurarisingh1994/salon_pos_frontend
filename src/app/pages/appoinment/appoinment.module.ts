import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { AppoinmentRoutingModule, routedComponents } from './appoinment-routing.module';
import { NbListModule, NbButtonModule, NbSpinnerModule, NbDialogModule, NbInputModule, NbActionsModule, NbToastrModule, NbSelectModule, NbCalendarModule, NbCheckboxModule, NbDatepickerModule } from '@nebular/theme';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { DemoUtilsModule } from './demo-utils/module';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';


@NgModule({
    imports: [
        ThemeModule,
        AppoinmentRoutingModule,
        NbListModule,
        NbButtonModule,
        NbSpinnerModule,
        NbInputModule,
        NbActionsModule,
        NbSelectModule,
        NbDialogModule.forChild(),
        NbToastrModule.forRoot(),
        NbCalendarModule,
        CalendarModule.forRoot({
            provide: DateAdapter,
            useFactory: adapterFactory
        }),
        DemoUtilsModule,
        NbCheckboxModule,
        NbDatepickerModule.forRoot(),
        NgxMaterialTimepickerModule.forRoot()
    ],
    declarations: [
       ...routedComponents,
    ],
})
export class AppoinmentModule { }
