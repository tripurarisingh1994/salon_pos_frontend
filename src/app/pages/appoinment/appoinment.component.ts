import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-appoinment',
  template: `<router-outlet></router-outlet>`,
})
export class AppoinmentComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
