import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../_service/common.service';

@Component({
  selector: 'ngx-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {

  constructor(private commonService: CommonService) { }

  msg = {
    reg_msg: '',
    bill_msg: '',
    appt_msg: ''
  }

  ngOnInit() {
    this.getMsg();
  }

  getMsg() {
    this.commonService.getMessage().subscribe(data => {
      if(data['message'] === 'success') {
        // console.log(data)
       this.msg.reg_msg  = data['data'][0].registration_msg
       this.msg.bill_msg = data['data'][0].bill_msg
       this.msg.appt_msg = data['data'][0].appointment_msg

      //  console.log(this.msg)
      }
      else {
        return;
      }
    })
  }

  updateMsg(): void {
    // console.log(reg_msg, bill_msg, appt_msg)
    this.commonService.updateMessage(this.msg.reg_msg, this.msg.bill_msg, this.msg.appt_msg).subscribe(data => {
      
      if(data['message'] === 'success') {
        this.getMsg();
      }
      else {
        return;
      }
    })
  }

}
