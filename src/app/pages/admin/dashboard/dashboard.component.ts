import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private myRoute: Router) { }

  ngOnInit() {
  }

  generalSetting(): void {
    this.myRoute.navigate(['pages/admin/general-setting']);
  }


  goCustomer(): void {
    this.myRoute.navigate(['pages/admin/customers']);
  }

  goItems(): void {
    this.myRoute.navigate(['pages/admin/items']);
  }

  goStaff(): void {
    this.myRoute.navigate(['pages/admin/staff']);
  }


  goInvoice(): void {
    this.myRoute.navigate(['pages/admin/invoice']);
  }


  goUsers(): void {
    this.myRoute.navigate(['pages/admin/users']);
  }

  goServiceCat(): void {
    this.myRoute.navigate(['pages/admin/service-category']);
  }

  goResources(): void {
    this.myRoute.navigate(['pages/admin/resources']);
  }


  goMessage(): void {
    this.myRoute.navigate(['pages/admin/message']);
  }


  goSales(): void {
    this.myRoute.navigate(['pages/admin/sales']);
  }

  goAppointment(): void {
    this.myRoute.navigate(['pages/admin/appointment-report'])
  }

}
