import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { CommonService } from '../../../_service/common.service';

@Component({
  selector: 'ngx-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.scss']
})
export class StaffComponent implements OnInit {

  loading: boolean = false

  settings = {
    add: {
      addButtonContent: 'Add New',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
  },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      staff_name: {
        title: 'Name',
        type:'string',
      }
    },
  };


  source: LocalDataSource = new LocalDataSource();
  
  constructor(private commonService: CommonService) { }

  ngOnInit() {
    this.gettingStaffDetails();
  }


  gettingStaffDetails() {

    this.commonService.gettingTheStaff().subscribe(data => {
      this.loading = true
       if(data['message'] === 'success') {
         let staff_list = [];
         staff_list = data['data'];
         this.source.load(staff_list);
         this.loading = false
       }
       else {
         this.loading = false
         return;
       }
    }, (err)=>{ this.loading = false })

  }


  onCreateConfirm(event): void {

    this.commonService.createStaff(event.newData.staff_name).subscribe(data => {

      if(data['message'] === 'success') {
        this.gettingStaffDetails();
         event.confirm.resolve();
      }
      else {
        event.confirm.reject();
      }

    },
     (err)=> {
      event.confirm.reject();
     }
     );

  }



  onEditConfirm(event): void {

    this.commonService.updateStaff(event.newData.id, event.newData.staff_name).subscribe(data=> {

      if(data['message'] === 'success') {
        this.gettingStaffDetails();
         event.confirm.resolve();
      }
      else {
        event.confirm.reject();
      }

    },
     (err)=> {
      event.confirm.reject();
     })
  
  }



  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {

      this.commonService.deleteStaff(event.data.id).subscribe(data => {
        // console.log(data);

        if(data['message'] === 'success') {
          this.gettingStaffDetails();
           event.confirm.resolve();
        }
        else {
          event.confirm.reject();
        }
      },   (err)=> {
        event.confirm.reject();
       }
      )
    } else {
      event.confirm.reject();
    }
  }

 

}
