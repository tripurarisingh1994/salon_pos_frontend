import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../_service/common.service';
import { LocalDataSource } from 'ng2-smart-table';


@Component({
  selector: 'ngx-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {


  loading: boolean = false
  currentUser;

  settings = {

    add: {
      addButtonContent: 'Add New',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
  },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      name: {
        title: 'Name',
        type: 'string',
      },
      email: {
        title: 'E-mail',
        type: 'string',
      },
      password: {
        title: 'Password',
        type: 'string',
      },
      active: {
        title: 'Active',
        type: 'string',
      },
      admin: {
        title: 'Admin',
        type: 'string',
      },
      last_login: {
        title: 'Last Login',
        type: 'string',
        editable: false
      },

    },
  };


  source: LocalDataSource = new LocalDataSource();
  
  constructor(private commonService: CommonService) { }

  ngOnInit() {
    this.getUserDetails();
  }

  getUserDetails(): void {
    this.commonService.gettingUsersDetails().subscribe(data => {
      this.loading = true

      if(data['message'] === 'success') {

        let users_list = [];
        users_list = data['data'];
        this.source.load(users_list);
        this.loading = false

      }
      else {

        this.loading = false
        return;

      }
    })
  }

// create new user
  onCreateConfirm(event): void {

    this.commonService.createUser(event.newData.name, event.newData.email, event.newData.password, event.newData.active, event.newData.admin).subscribe(data => {

      if(data['message'] === 'success') {
        this.getUserDetails();
         event.confirm.resolve();
      }
      else {
        event.confirm.reject();
      }

    },
     (err)=> {
      event.confirm.reject();
     }
     );

  }


  

  onEditConfirm(event): void {

    this.commonService.updateUserDetails(event.newData.id, event.newData.name, event.newData.email, event.newData.password, event.newData.active, event.newData.admin).subscribe(data=> {

      if(data['message'] === 'success') {
        this.getUserDetails();
         event.confirm.resolve();
      }
      else {
        event.confirm.reject();
      }

    },
     (err)=> {
      event.confirm.reject();
     })
  
  }


  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {

      this.commonService.deleteUserDetails(event.data.id).subscribe(data => {
        console.log(data);

        if(data['message'] === 'success') {
          this.getUserDetails();
           event.confirm.resolve();
        }
        else {
          event.confirm.reject();
        }
      },   (err)=> {
        event.confirm.reject();
       }
      )
    } else {
      event.confirm.reject();
    }
  }

}
