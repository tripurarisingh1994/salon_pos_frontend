import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { CommonService } from '../../../_service/common.service';

@Component({
  selector: 'ngx-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss']
})
export class InvoiceComponent implements OnInit {

  loading: boolean = false

  settings = {
      actions: {
            custom: [{ name: 'print', title: '<i class="fas fa-print"></i>' }],
        },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      name: {
        title: 'Customer Name',
        type:'string',
      },
      email: {
        title: 'E-mail',
        type: 'string',
      },
      total_price: {
        title: 'Total Price',
        type: 'string',
      },
      created_at: {
        title: 'Created At',
        type: 'string',
      },
      updated_at: {
        title: 'Updated At',
        type: 'string',
      },
    },
  };


  source: LocalDataSource = new LocalDataSource();
  
  constructor(private commonService: CommonService) { }

  ngOnInit() {
   this.commonService.gettingInvoiceDetails().subscribe(data => {
     this.loading = true
      if(data['message'] === 'success') {
        let invoice_list = [];
        invoice_list = data['data'];
        this.source.load(invoice_list);
        this.loading = false
      }
      else {
        this.loading = false
        return;
      }
   }, (err)=>{ this.loading = false })
  }

 

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  onEditConfirm(event): void {
    console.log('edit');
    console.log(event);
   event.confirm.resolve();
  }

  print(event): void {
    console.log(event)
  }

}
