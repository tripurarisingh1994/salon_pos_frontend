import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { CommonService } from '../../../_service/common.service';

@Component({
  selector: 'ngx-general-setting',
  templateUrl: './general-setting.component.html',
  // changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./general-setting.component.scss']
})
export class GeneralSettingComponent implements OnInit {

  general_setting = {
    shopname :'',
    mobile   :'',
    address  :'',
    pincode  :'',
    website  :''
  }

  currentUser;


  constructor(
              private commonService: CommonService, 
              private changeDetector: ChangeDetectorRef) {

              this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
   }

  ngOnInit() {
    this.gettingGeneralSetting();
  }

  gettingGeneralSetting() {
   this.commonService.gettingGeneralSetting(this.currentUser.client_id).subscribe(data => {
    // console.log(data);

    if(data['message'] === 'success') {

      localStorage.setItem('gs', JSON.stringify(data['data'][0]));
      this.general_setting.shopname = data['data'][0].shop_name
      this.general_setting.mobile   = data['data'][0].mobile
      this.general_setting.address  = data['data'][0].address
      this.general_setting.pincode  = data['data'][0].pin_code
      this.general_setting.website  = data['data'][0].website

      // this.changeDetector.detectChanges();
    }
    else {
      return;
    }

   });
  }

  updateGeneralSetting(): void {

    this.commonService.savingGeneralSetting(this.currentUser.client_id, this.general_setting).subscribe(data => {
      // console.log(data);

      if(data['message'] === 'success') {
          this.gettingGeneralSetting();
      }
      else {
        return;
      }
    })

  }

}
