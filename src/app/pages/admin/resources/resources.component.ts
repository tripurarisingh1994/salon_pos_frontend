import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { CommonService } from '../../../_service/common.service';


@Component({
  selector: 'ngx-resources',
  templateUrl: './resources.component.html',
  styleUrls: ['./resources.component.scss']
})
export class ResourcesComponent implements OnInit {

  loading: boolean = false
  currentUser;

  settings = {

    add: {
      addButtonContent: 'Add New',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
  },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      res_name: {
        title: 'Name',
        type: 'string',
      },
    },
  };


  source: LocalDataSource = new LocalDataSource();

  constructor(private commonService: CommonService) { }

  ngOnInit() {
    this.getResources();
  }

  getResources() {
    this.commonService.gettingTheResources().subscribe(data => {

      this.loading = true

      if(data['message'] === 'success') {

        let resource_list = [];
        resource_list = data['data'];
        this.source.load(resource_list);
        this.loading = false

      }
      else {

        this.loading = false
        return;

      }
    });
  }


  onCreateConfirm(event): void {

    this.commonService.createResource(event.newData.res_name).subscribe(data => {

      if(data['message'] === 'success') {
        this.getResources();
         event.confirm.resolve();
      }
      else {
        event.confirm.reject();
      }

    },
     (err)=> {
      event.confirm.reject();
     }
     );

  }



  onEditConfirm(event): void {

    this.commonService.updateResources(event.newData.id, event.newData.res_name).subscribe(data=> {

      if(data['message'] === 'success') {
        this.getResources();
         event.confirm.resolve();
      }
      else {
        event.confirm.reject();
      }

    },
     (err)=> {
      event.confirm.reject();
     })
  
  }






  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {

      this.commonService.deleteResources(event.data.id).subscribe(data => {
        console.log(data);

        if(data['message'] === 'success') {
          this.getResources();
           event.confirm.resolve();
        }
        else {
          event.confirm.reject();
        }
      },   (err)=> {
        event.confirm.reject();
       }
      )
    } else {
      event.confirm.reject();
    }
  }

}
