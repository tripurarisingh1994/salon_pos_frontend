import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../_service/common.service';
import { ngxCsv } from 'ngx-csv/ngx-csv';

@Component({
  selector: 'ngx-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.scss']
})
export class SalesComponent implements OnInit {

  saleData:any = []
  constructor(private commonService: CommonService) { }

  ngOnInit() {
    
  }

  go(fromD, toD): void {
    this.commonService.getSaleReport(fromD, toD).subscribe(data => {
      if(data['message'] === 'success') {
        this.saleData = data['data']
      }
      else return
    })
  }

  export(fromD, toD): void {
    const currentDate = new Date()
    const d = currentDate.getDate()
    const m = currentDate.getMonth() + 1
    const y = currentDate.getFullYear()

    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true, 
      showTitle: true,
      title: 'Sale Report '+fromD+' to '+toD,
      useBom: true,
      noDownload: false,
      headers: ['Customer Name', 'Customer Mobile', 'Service Taken', 'Cost Price', 'Discount', 'Payment Mode', 'Amount Receive']
    };

    new ngxCsv(this.saleData, 'sale_report '+m+'-'+d+'-'+y, options);
    
  }

}
