import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../_service/common.service';
import { LocalDataSource } from 'ng2-smart-table';

@Component({
  selector: 'ngx-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss']
})
export class CustomersComponent implements OnInit {

  loading: boolean = false
  currentUser;

  settings = {

    add: {
      addButtonContent: 'Add New',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
  },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      name: {
        title: 'Name',
        type: 'string',
      },
      email: {
        title: 'E-mail',
        type: 'string',
      },
      mob: {
        title: 'Mobile',
        type: 'string',
      },
    },
  };


  source: LocalDataSource = new LocalDataSource();
  
  constructor(private commonService: CommonService) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
   }

  ngOnInit() {
   this.getCustomerList();
  }

  getCustomerList() {
    this.commonService.getCustomerList().subscribe(data => {
      this.loading = true
       if(data['message'] === 'success') {
         let customer_list = [];
         customer_list = data['data'];
         this.source.load(customer_list);
         this.loading = false
       }
       else {
         this.loading = false
         return;
       }
    }, (err)=>{ this.loading = false })
  }

 

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {

      this.commonService.deleteCustomerDetails(this.currentUser.client_id, event.data.id).subscribe(data => {
        // console.log(data);

        if(data['message'] === 'success') {
          this.getCustomerList();
           event.confirm.resolve();
        }
        else {
          event.confirm.reject();
        }
      },   (err)=> {
        event.confirm.reject();
       }
      )
    } else {
      event.confirm.reject();
    }
  }

  onEditConfirm(event): void {
    // console.log('edit');
    // console.log(event);

    this.commonService.updatingCustomerDetails(this.currentUser.client_id, event.newData.id, event.newData.name, event.newData.email, event.newData.mob).subscribe(data=> {
      // console.log(data);

      if(data['message'] === 'success') {
        this.getCustomerList();
         event.confirm.resolve();
      }
      else {
        event.confirm.reject();
      }

    },
     (err)=> {
      event.confirm.reject();
     })
  
  }

  onCreateConfirm(event): void {

    this.commonService.savingCustomerDetails(this.currentUser.client_id, event.newData.name, event.newData.email, event.newData.mob)
    
    .subscribe(data => {

      if(data['message'] === 'success') {
        this.getCustomerList();
         event.confirm.resolve();
      }
      else {
        event.confirm.reject();
      }

    },
     (err)=> {
      event.confirm.reject();
     }
     );

    // event.confirm.resolve();
  }

}
