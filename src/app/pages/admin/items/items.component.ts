import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { CommonService } from '../../../_service/common.service';

@Component({
  selector: 'ngx-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss']
})
export class ItemsComponent implements OnInit {
  currentUser: any;
  loading: boolean = false
  settings = {};
  dropdown_list = [];

  source: LocalDataSource = new LocalDataSource();
  
  constructor(private commonService: CommonService) {

    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.gettingItems();
   }

  ngOnInit() {
    this.loadSetting();
  }


  getFoodCat() {
    this.commonService.getFoodCategory().subscribe(data => {
           
      if(data['message'] === 'success') {

         this.dropdown_list = [];

        data['data'].forEach(element => {
          this.dropdown_list.push({title: element.name, value: element.id})
        });

        this.loadSetting(this.dropdown_list);
       }
       else {
         return;
       }
     })
  }

  gettingItems() {
    this.commonService.gettingItemsDetails(this.currentUser.client_id,'').subscribe(data => {
     
      this.loading = true

       if(data['message'] === 'success') {
         let items_list = [];
         items_list = data['data'];

         this.source.load(items_list);

         this.getFoodCat();

         this.loading = false 
       }
       else {
        this.getFoodCat();
         this.loading = false
         return;
       }
    }, (err)=>{ this.loading = false })
  }


  onCreateConfirm(event): void {

    this.commonService.createNewItem(this.currentUser.client_id, event.newData).subscribe(data => {

      if(data['message'] === 'success') {
        this.gettingItems();
         event.confirm.resolve();
      }
      else {
        event.confirm.reject();
      }

    },
     (err)=> {
      event.confirm.reject();
     })
     
  }
 

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {

      this.commonService.deleteItemDetails(this.currentUser.client_id, event.data).subscribe(data => {
  
        if(data['message'] === 'success') {
          this.gettingItems();
           event.confirm.resolve();
        }
        else {
          event.confirm.reject();
        }
  
      },
       (err)=> {
        event.confirm.reject();
       })
     
    } else {
      event.confirm.reject();
    }
  }

  onEditConfirm(event): void {
    console.log(event.newData)
    this.commonService.updateItemDetails(this.currentUser.client_id, event.newData).subscribe(data=> {

      if(data['message'] === 'success') {
        this.gettingItems();
         event.confirm.resolve();
      }
      else {
        event.confirm.reject();
      }

    },
     (err)=> {
      event.confirm.reject();
     })

  }

  loadSetting(list=[]) {

    this.settings = {
      add: {
        addButtonContent: 'Add New',
        createButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
        confirmCreate: true,
    },edit: {
        editButtonContent: '<i class="nb-edit"></i>',
        saveButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
        confirmSave: true,
      },
      delete: {
        deleteButtonContent: '<i class="nb-trash"></i>',
        confirmDelete: true,
      },
  
      columns: {
        category_name: {
          title: 'Cat Name',
          editor: {
            type: 'list',
            config: {
              list: list
            }
        }
      },
        item_name: {
          title: 'Item Name',
          type: 'string',
        },
        price: {
          title: 'Price',
          type: 'number',
        },
        hsn_code: {
          title: 'HSN Code',
          type: 'string'
        },
        gst_rate: {
          title: 'GST Rate',
          type: 'number'
        }
      },
    };

  }

}
