import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
import { AdminRoutingModule, routedComponents } from './admin-routing.module';
import { NbListModule, NbButtonModule, NbSpinnerModule, NbDialogModule, NbInputModule, NbActionsModule, NbToastrModule, NbSelectModule, NbDatepickerModule } from '@nebular/theme';

@NgModule({
    imports: [
        ThemeModule,
        AdminRoutingModule,
        NbListModule,
        NbButtonModule,
        NbSpinnerModule,
        NbInputModule,
        NbActionsModule,
        NbSelectModule,
        NbDialogModule.forChild(),
        NbToastrModule.forRoot(),
        Ng2SmartTableModule,
        NbDatepickerModule,
    ],
    declarations: [
       ...routedComponents,
    ]
})
export class AdminModule { }
