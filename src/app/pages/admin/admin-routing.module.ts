import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { GeneralSettingComponent } from './general-setting/general-setting.component';
import { CustomersComponent } from './customers/customers.component';
import { ItemsComponent } from './items/items.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { StaffComponent } from './staff/staff.component';
import { UsersComponent } from './users/users.component';
import { ServiceCategoryComponent } from './service-category/service-category.component';
import { ResourcesComponent } from './resources/resources.component';
import { MessageComponent } from './message/message.component';
import { SalesComponent } from './sales/sales.component';
import { AppointmentReportComponent } from './appointment-report/appointment-report.component';


const routes: Routes = [{
  path: '',
  component: AdminComponent,
  children: [
    {
      path: '',
      component: DashboardComponent,
   },
  {
     path: 'dashboard',
     component: DashboardComponent,
  },
  {
    path: 'general-setting',
    component: GeneralSettingComponent,
 },
  {
    path: 'customers',
    component: CustomersComponent,
 },
  {
    path: 'items',
    component: ItemsComponent,
 },
  {
    path: 'staff',
    component: StaffComponent,
 },
  {
    path: 'invoice',
    component: InvoiceComponent,
 },
 {
  path: 'users',
  component: UsersComponent,
},
{
  path: 'service-category',
  component: ServiceCategoryComponent,
},
{
  path: 'resources',
  component: ResourcesComponent,
},
{
  path: 'message',
  component: MessageComponent,
},
{
  path: 'sales',
  component: SalesComponent,
},
{
  path: 'appointment-report',
  component: AppointmentReportComponent,
}
],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule { }

export const routedComponents = [
  AdminComponent,
  DashboardComponent,
  GeneralSettingComponent,
  CustomersComponent,
  ItemsComponent,
  StaffComponent,
  InvoiceComponent,
  UsersComponent,
  ServiceCategoryComponent,
  ResourcesComponent,
  MessageComponent,
  SalesComponent,
  AppointmentReportComponent
];
