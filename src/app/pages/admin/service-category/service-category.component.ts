import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../_service/common.service';
import { LocalDataSource } from 'ng2-smart-table';


@Component({
  selector: 'ngx-service-category',
  templateUrl: './service-category.component.html',
  styleUrls: ['./service-category.component.scss']
})
export class ServiceCategoryComponent implements OnInit {


  loading: boolean = false
  currentUser;

  settings = {

    add: {
      addButtonContent: 'Add New',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
  },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      name: {
        title: 'Name',
        type: 'string',
      },
    },
  };


  source: LocalDataSource = new LocalDataSource();
  constructor(private commonService: CommonService) { }

  ngOnInit() {
    this.getttingFoodCategory();
  }

  getttingFoodCategory(): void {
    this.commonService.getFoodCategory().subscribe(data => {

      this.loading = true

      if(data['message'] === 'success') {

        let category_list = [];
        category_list = data['data'];
        this.source.load(category_list);
        this.loading = false

      }
      else {

        this.loading = false
        return;

      }
    });
  }


  onCreateConfirm(event): void {

    this.commonService.createFoodCategory(event.newData.name).subscribe(data => {

      if(data['message'] === 'success') {
        this.getttingFoodCategory();
         event.confirm.resolve();
      }
      else {
        event.confirm.reject();
      }

    },
     (err)=> {
      event.confirm.reject();
     }
     );

  }



  onEditConfirm(event): void {

    this.commonService.updateFoodCategory(event.newData.id, event.newData.name).subscribe(data=> {

      if(data['message'] === 'success') {
        this.getttingFoodCategory();
         event.confirm.resolve();
      }
      else {
        event.confirm.reject();
      }

    },
     (err)=> {
      event.confirm.reject();
     })
  
  }






  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {

      this.commonService.deleteFoodCategory(event.data.id).subscribe(data => {
        console.log(data);

        if(data['message'] === 'success') {
          this.getttingFoodCategory();
           event.confirm.resolve();
        }
        else {
          event.confirm.reject();
        }
      },   (err)=> {
        event.confirm.reject();
       }
      )
    } else {
      event.confirm.reject();
    }
  }

}
