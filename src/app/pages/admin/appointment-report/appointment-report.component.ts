import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../_service/common.service';
import { ngxCsv } from 'ngx-csv/ngx-csv';

@Component({
  selector: 'ngx-appointment-report',
  templateUrl: './appointment-report.component.html',
  styleUrls: ['./appointment-report.component.scss']
})
export class AppointmentReportComponent implements OnInit {

  appointmentData:any = [];

  constructor(private commonService: CommonService) { }

  ngOnInit() {
  }

  go(fromD, toD): void {
    this.commonService.getAppointmentReport(fromD, toD).subscribe(data => {
      if(data['message'] === 'success') {
        this.appointmentData = data['data']
      }
      else return
    });
  }


  export(fromD, toD): void {
    const currentDate = new Date()
    const d = currentDate.getDate()
    const m = currentDate.getMonth() + 1
    const y = currentDate.getFullYear()

    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true, 
      showTitle: true,
      title: 'Appointment Report '+fromD+' to '+toD,
      useBom: true,
      noDownload: false,
      headers: ['Customer Name', 'Customer Mobile', 'Service Offered', 'Service Price', 'Staff Name', 'Book Date']
    };

    new ngxCsv(this.appointmentData, 'appointment_report '+m+'-'+d+'-'+y, options);
  
  }

}
