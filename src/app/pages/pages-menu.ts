import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'nb-home',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'Invoice',
    icon: 'nb-snowy-circled',
    link: '/pages/table',
  },
  {
    title: 'Appoinment',
    icon: 'nb-compose',
    link: '/pages/appoinment',
  },
  {
    title: 'Admin',
    icon: 'nb-person',
    link: '/pages/admin',
  },
];
