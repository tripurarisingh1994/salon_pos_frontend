import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {

  constructor(private http: HttpClient, private configService: ConfigService) { }

  public login(email:string, password:string) {

    const data = new FormData();
    data.append('email', email);
    data.append('password', password);
    return this.http.post(`${this.configService.url}/index.php/auth/doLogin`, data);
  }
}
