import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConfigService } from './config.service';


interface ICart {
  id:number,
  name:string,
  price:number,
  qty:number
}


interface IAppoinment {
  customerDetails: Object,
  serviceDetails: Object,
  resource_id: number,
  staff_id: number,
  staff_name: string,
  servicing_startTime: string,
  servicing_endTime: string,
  booked_date: string,
  booked_time_24H:string,
}



@Injectable({
  providedIn: 'root',
})

export class CommonService {


  /** Storing the food items */
 public items_cart: ICart[] = []; 


 /** Storing the appoinment data 
  * (customer, resources, staff, service_item, booktime, service duration)
  */

  public appointment_details: IAppoinment[] = [];
  currentUser: any; 

  public booked_appointment_details = [];

 constructor(private http: HttpClient, private configService: ConfigService) {
  this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
}



 
  /**
  * Calculate the Total Price 
  * items added in cart 
   */
 totalPriceOfFoodItems(discount?:number): number {

    if(this.items_cart.length > 0) {

      let price:number = 0;

        for(let i=0; i<this.items_cart.length; i++) {
           price += this.items_cart[i].price * this.items_cart[i].qty;
        }

        if(discount) return price - (price * discount/100);
        else  return price;
    }

    return 0;
  }  



  
  /** Getting Food Category */

  getFoodCategory() {
    return this.http.get(`${this.configService.url}/index.php/common/getFoodCategory?client_id=${this.currentUser.client_id}`);
  }

  // create the food category
  createFoodCategory(cat_name) {
    return this.http.post(`${this.configService.url}/create-food-category`,{client_id : this.currentUser.client_id, cat_name}, {
      headers: new HttpHeaders({
        'Content-Type' : 'application/x-www-form-urlencoded',
      })
    })
  }

  // update the food category
  updateFoodCategory(cat_id, cat_name) {
    return this.http.post(`${this.configService.url}/update-food-category`,{client_id : this.currentUser.client_id, cat_id, cat_name}, {
      headers: new HttpHeaders({
        'Content-Type' : 'application/x-www-form-urlencoded',
      })
    })
  }

  // delete the food category
  deleteFoodCategory(cat_id) {
    return this.http.post(`${this.configService.url}/delete-food-category`,{client_id : this.currentUser.client_id, cat_id}, {
      headers: new HttpHeaders({
        'Content-Type' : 'application/x-www-form-urlencoded',
      })
    })
  }

  /** Getting the food items by food category id */
  getFoodItemsByFoodCategory(id) {
    return this.http.get(`${this.configService.url}/index.php/common/getFoodItemsByFoodCatId?id=${id}&client_id=${this.currentUser.client_id}`);
  }

  /** Getting the price of requested food item by food item id */

  getFoodItemPriceByFoodItemId(id) {
    return this.http.get(`${this.configService.url}/index.php/common/getFoodItemPriceByFoodItemId?id=${id}&client_id=${this.currentUser.client_id}`);
  }




  /** Saving Customer Details */
  savingCustomerDetails(client_id, name, email, mob) {
      const customer = new FormData();

      customer.append('client_id', client_id);
      customer.append('name', name);
      customer.append('email', email);
      customer.append('mob', mob);

      return this.http.post(`${this.configService.url}/index.php/common/saveCustomersDetails`, customer);
  }

  // Updating Customer Details
  updatingCustomerDetails(client_id, id, name, email, mob) {

    const customer = new FormData();

      customer.append('client_id', client_id);
      customer.append('id', id);
      customer.append('name', name);
      customer.append('email', email);
      customer.append('mob', mob);

    return this.http.post(`${this.configService.url}/update-customer`, customer);
  }

  // Deleting Customer Details
  deleteCustomerDetails(client_id, id) {

    const data = new FormData();
    data.append('client_id', client_id)
    data.append('id', id)

    return this.http.post(`${this.configService.url}/delete-customer`, data);
  }


  /** Search the customer */
  
  searchCustomer(client_id, query) {
    return this.http.get(`${this.configService.url}/index.php/common/searchCustomerByName?query=${query}&client_id=${client_id}`);
  }

  /** Search the Food Items by item name */
  searchFoodItems(query) {
    return this.http.get(`${this.configService.url}/index.php/common/searchFoodItemsByName?query=${query}&client_id=${this.currentUser.client_id}`)
  }

  /** Insert new invoice Data */

  insertNewInvoice(customer_id, amount_receive, paymentMode, _discount) {

    const itemPriceArray = this.items_cart.map(items => items.price)
    const itemNameArray  = this.items_cart.map(items => items.name)

    const cost_price = itemPriceArray.reduce((i,j) => i + j, 0)

    const newInvoice = new FormData();
    newInvoice.append('client_id', this.currentUser.client_id);
    newInvoice.append('customer_id', customer_id);
    newInvoice.append('amount_receive', amount_receive);
    newInvoice.append('payment_mode', paymentMode);
    newInvoice.append('discount', _discount);
    newInvoice.append('cost_price', cost_price.toString());
    newInvoice.append('service_taken', itemNameArray.toString());

    return this.http.post(`${this.configService.url}/index.php/common/insertInvoiceData`, newInvoice);
  }

  /** Save Items, category wise and it's price */
  saveItemsAndItsPrice(cat_id, item_name, price, hsn_code, gst_rate) {
    const itemData = new FormData();
    itemData.append('client_id', this.currentUser.client_id);
    itemData.append('cat_id', cat_id);
    itemData.append('item_name', item_name);
    itemData.append('price', price);
    itemData.append('hsn_code', hsn_code);
    itemData.append('gst_rate', gst_rate);
    
    return this.http.post(`${this.configService.url}/index.php/common/saveItemsAndItsPrice`, itemData);
  }

  /** Getting Customer List */

  getCustomerList() {
    return this.http.get(`${this.configService.url}/index.php/common/gettingCustomerData?client_id=${this.currentUser.client_id}`);
  }

  /** Getting Items Details */

  gettingItemsDetails(client_id, query) {
    return this.http.get(`${this.configService.url}/index.php/common/gettingItemsDetails?query=${query}&client_id=${client_id}`);
  }

  /** Getting Users Details */

  gettingUsersDetails() {
    return this.http.post(`${this.configService.url}/get-user-details`,{client_id : this.currentUser.client_id }, {
      headers: new HttpHeaders({
        'Content-Type' : 'application/x-www-form-urlencoded',
      })
    })
  }

 // create new user

 createUser(name, email, password, active, admin) {
  return this.http.post(`${this.configService.url}/create-user`,{client_id : this.currentUser.client_id, name, email, password, active, admin}, {
    headers: new HttpHeaders({
      'Content-Type' : 'application/x-www-form-urlencoded',
    })
  })
 }


 // update user details
 updateUserDetails(user_id, name, email, password, active, admin) {
  return this.http.post(`${this.configService.url}/update-user`,{client_id : this.currentUser.client_id, user_id, name, email, password, active, admin}, {
    headers: new HttpHeaders({
      'Content-Type' : 'application/x-www-form-urlencoded',
    })
  })
 }


 // delete user details
 deleteUserDetails(user_id) {
  return this.http.post(`${this.configService.url}/delete-user`,{client_id : this.currentUser.client_id, user_id}, {
    headers: new HttpHeaders({
      'Content-Type' : 'application/x-www-form-urlencoded',
    })
  })
 }



  /** Getting Invoice Deails */

  gettingInvoiceDetails() {
    return this.http.get(`${this.configService.url}/index.php/common/gettingInvoiceDetails?client_id=${this.currentUser.client_id}`)
  }

  /** Getting The Resources */

  gettingTheResources() {
   return this.http.get(`${this.configService.url}/index.php/common/gettingResources?client_id=${this.currentUser.client_id}`)
  }

  // create new resources
  createResource(res_name) {
    return this.http.post(`${this.configService.url}/create_resource`,{client_id : this.currentUser.client_id, res_name}, {
      headers: new HttpHeaders({
        'Content-Type' : 'application/x-www-form-urlencoded',
      })
    })
  }

  // update the resources
  updateResources(res_id, res_name) {
    return this.http.post(`${this.configService.url}/update-resource`,{client_id : this.currentUser.client_id, res_id, res_name}, {
      headers: new HttpHeaders({
        'Content-Type' : 'application/x-www-form-urlencoded',
      })
    })
  }

  // delete the resources
  deleteResources(res_id) {
    return this.http.post(`${this.configService.url}/delete-resource`,{client_id : this.currentUser.client_id, res_id}, {
      headers: new HttpHeaders({
        'Content-Type' : 'application/x-www-form-urlencoded',
      })
    })
  }

  /** Getting The Staff */
  gettingTheStaff() {
    return this.http.get(`${this.configService.url}/index.php/common/gettingStaffList?client_id=${this.currentUser.client_id}`)
  }

  // create staff
  createStaff(staff_name) {
    return this.http.post(`${this.configService.url}/create-staff`,{client_id : this.currentUser.client_id, staff_name}, {
      headers: new HttpHeaders({
        'Content-Type' : 'application/x-www-form-urlencoded',
      })
    })
  }

  // update staff details
  updateStaff(staff_id, staff_name) {
    return this.http.post(`${this.configService.url}/update-staff`,{client_id : this.currentUser.client_id, staff_id, staff_name}, {
      headers: new HttpHeaders({
        'Content-Type' : 'application/x-www-form-urlencoded',
      })
    })
  }

  // delete staff
  deleteStaff(staff_id) {
    return this.http.post(`${this.configService.url}/delete-staff`,{client_id : this.currentUser.client_id, staff_id}, {
      headers: new HttpHeaders({
        'Content-Type' : 'application/x-www-form-urlencoded',
      })
    })
  }

  /** Saving Appointment Details */
  savingAppointmentDetails() {
    return this.http.post(`${this.configService.url}/index.php/common/savingAppointmentDetails`,{client_id: this.currentUser.client_id, appointment: this.appointment_details }, {
      headers: new HttpHeaders({
        'Content-Type' : 'application/json',
        'Accept': 'application/json',
      })
    })

  }

  // Fetch appointment details
  fetchAppointmentDetails() {
    return this.http.get(`${this.configService.url}/fetch-appointment-details?client_id=${this.currentUser.client_id}`)
  }


  // Save the general Setting
  savingGeneralSetting(client_id, g_setting) {
    return this.http.post(`${this.configService.url}/save-general-setting`,{client_id, gs:g_setting}, {
      headers: new HttpHeaders({
        'Content-Type' : 'application/x-www-form-urlencoded',
      })
    })
  }


  // Getting General Setting
  gettingGeneralSetting(client_id) {
    return this.http.get(`${this.configService.url}/getting-general-setting?client_id=${client_id}`)
  }


  // create new item with item's category and item price
  createNewItem(client_id, item_details) {
    return this.http.post(`${this.configService.url}/create-new-item`,{client_id, item_details}, {
      headers: new HttpHeaders({
        'Content-Type' : 'application/x-www-form-urlencoded',
      })
    })
  }

  // update items details
  updateItemDetails(client_id, item_details) {
    return this.http.post(`${this.configService.url}/update-item`,{client_id, item_details}, {
      headers: new HttpHeaders({
        'Content-Type' : 'application/x-www-form-urlencoded',
      })
    })
  }


  // delete items details
  deleteItemDetails(client_id, item_details) {
    return this.http.post(`${this.configService.url}/delete-item`,{client_id, item_details}, {
      headers: new HttpHeaders({
        'Content-Type' : 'application/x-www-form-urlencoded',
      })
    })
  }


  // update msg
  updateMessage(reg_msg, bill_msg, appt_msg) {
    return this.http.post(`${this.configService.url}/update-msg`,{client_id : this.currentUser.client_id, reg_msg, bill_msg, appt_msg}, {
      headers: new HttpHeaders({
        'Content-Type' : 'application/x-www-form-urlencoded',
      })
    })
  }


  // get message
  getMessage() {
    return this.http.get(`${this.configService.url}/get-msg?client_id=${ this.currentUser.client_id }`)
  }

  // Report Section
  getSaleReport(fd, td) {
    return this.http.get(`${this.configService.url}/sale-report?fd=${fd}&td=${td}&client_id=${ this.currentUser.client_id }`)
  }

  exportSaleReport(fd, td) {
    return this.http.get(`${this.configService.url}/export-sale-report?fd=${fd}&td=${td}&client_id=${ this.currentUser.client_id }`)
  }

  getAppointmentReport(fd, td) {
    return this.http.get(`${this.configService.url}/appointment-report?fd=${fd}&td=${td}&client_id=${ this.currentUser.client_id }`)
  }

}
