import { NgModule } from '@angular/core';

import { ThemeModule } from '../@theme/theme.module';
import { AuthRoutingModule, routedComponents } from './auth-routing.module';
import { NbToastrModule, NbSpinnerModule } from '@nebular/theme';

@NgModule({
    imports: [
        ThemeModule,
        AuthRoutingModule,
        NbSpinnerModule,
        NbToastrModule.forRoot(),
    ],
    declarations: [
        ...routedComponents,
    ],
})
export class AuthModule { }
