import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../_service/auth.service';
import { Router } from '@angular/router';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  loading: boolean = false;

  constructor(private auth: AuthService, private myRoute: Router, private toastrService: NbToastrService) { }

  ngOnInit() {

  }

  submit(email, password) {
    if(email != "" || password != "") {
      this.loading = true

      this.auth.login(email, password).subscribe( data => {
        if (data['message'] === 'success') {
          // console.log(JSON.stringify(data));
          localStorage.setItem('currentUser', JSON.stringify(data));
          if(data['admin'] === 2)  {
            console.log('admin 2');
            this.myRoute.navigate(['admin-dashboard/dashboard']);
          } 
          else {
            if(data['admin'] === 1) {
              localStorage.setItem('isAdmin',"1");
            }
            this.loading = false
            this.myRoute.navigate(['pages/dashboard']);
          }
        } 
        else {
          this.loading = false
          this.toastrService.warning('Warning', 'Email or Password is worong!');
        }
      }, (err) => this.loading = false );
    }
    else {
      return;
    }
     
  }

}
